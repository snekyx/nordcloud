import Dependencies._

ThisBuild / scalaVersion     := "2.12.9"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

val sparkVersion = "2.4.3"

lazy val root = (project in file("."))
  .settings(
    name := "nordcloud",
    resolvers += "jitpack" at "https://jitpack.io",
    libraryDependencies ++= Seq(scalaTest % Test,
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "com.github.wookietreiber" %% "scala-chart" % "latest.integration"
    )
)

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
