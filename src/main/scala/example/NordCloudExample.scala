package example

import example.CurrencyConversion._
import org.apache.spark.sql.SparkSession

object NordCloudExample extends App with ImageGenerator {
  val spark = SparkSession.builder.
           master("local")
           .appName("nordCloudExample")
           .getOrCreate()

  val df = spark.read
                .option("header", "true")
                .csv("src/main/resources/btc.csv")
                .convertToEur()
  
  df.createTempView("btc")

  // write last years data to CSV File
  generateLastYearsBtc(spark)

  // write to console the total amount of coins generated last year
  totalCoinsLastYear(spark)

  // print metrics to Std.out
  metricsForLastYear(spark)

  //generateImage
  generateImage(df, spark)

  spark.stop()

  def generateLastYearsBtc(spark: SparkSession) = {
    spark.sql("""
    select date, `marketCap(EUR)`, `price(EUR)`, generatedCoins, paymentCount 
    from btc 
    where date > (select date_sub(max(date), 365) from btc)
    """)
         .repartition(1)
         .write
         .option("header", true)
         .csv("src/main/resources/btc-last-year.csv")
  } 

  def totalCoinsLastYear(spark: SparkSession) = {
    spark.sql("select sum(generatedCoins) from btc where date > (select date_sub(max(date), 365) from btc)").show
  }

  def metricsForLastYear(spark: SparkSession) = {
    Seq("generatedCoins", "`marketCap(EUR)`", "`price(EUR)`", "paymentCount").foreach(column => {
      spark.sql(s"select round(min(${column}) * 100 / 5) * 5 / 100 as min, round(max(${column}) * 100 / 5) * 5 / 100 as max, round(avg(${column}) /5 * 100) * 5 / 100 as average from btc where date > (select date_sub(max(date), 365) from btc)").show
    })
  }
}
