package example

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import services.ExchangeRateService

object CurrencyConversion {
    implicit class TransformationMethods(df: DataFrame) extends ExchangeRateService {
      def convertToEur(): DataFrame = {
        val exchangeRate = convert("USD", "EUR")

        def inEur(srcColName: String, resultColName: String)(dataFrame: DataFrame) = {
          dataFrame.withColumn(resultColName, round(col(srcColName) * exchangeRate, 4))
        }
        df.transform(inEur("marketcap(USD)","marketcap(EUR)"))
          .transform(inEur("price(USD)","price(EUR)"))
      }
  }
}