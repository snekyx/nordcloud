package services 

trait ExchangeRateService {
    def convert(currencyFrom: String, currencyTo: String): Float = {
        (currencyFrom, currencyTo) match {
            case ("USD", "EUR") => 0.87f
            case (_,_) => 1.0f
        }
        
    }
}