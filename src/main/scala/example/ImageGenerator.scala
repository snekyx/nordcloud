package example

import org.apache.spark.sql.{DataFrame, SparkSession}

trait ImageGenerator extends scalax.chart.module.Charting {
  def generateImage(df: DataFrame, spark: SparkSession) = {

    val prices = spark
                    .sql("select `price(EUR)` from btc where date > (select date_sub(max(date), 365) from btc)")
                    .collect()
                    .map(_(0).toString.toDouble)
                    .toList

    val x = (1 until prices.length).map(_.toDouble)

    val data = prices.zipWithIndex.map({case (p, i) => (i, p)})
    
    val chart = XYLineChart(data)
    chart.saveAsPNG("src/main/resources/btc-last-year.png")
  }
}